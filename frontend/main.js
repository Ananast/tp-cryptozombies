  import "./style.css";

  // 1. Import modules.
  import { createPublicClient, http, getContract, formatUnits, createWalletClient, custom, parseUnits, parseAbiItem, getAddress, parseEther} from "viem";
  import { goerli } from "viem/chains";
  import { CRYPTOZOMBIES } from "./abi/CryptoZombies";

  // 2. Set up your client with desired chain & transport.
  const publicClient = createPublicClient({
    chain: goerli,
    transport: http(),
  });

  const [account] = await window.ethereum.request({ method: "eth_requestAccounts" });
  const walletClient = createWalletClient({
    account,
    chain: goerli,
    transport: custom(window.ethereum),
  });

  const zombieContract = getContract({
    address : "0x53B9bF271971D780E2543e5658f2f9f9819095a5",
    abi: CRYPTOZOMBIES,
    publicClient,
    walletClient,
  });

  // 3. Consume an action!

  //const create = await zombieContract.write.createRandomZombie(["Anast"]);
  const ownerAddress = await zombieContract.read.owner();
  const blockNumber = await publicClient.getBlockNumber();
  const balanceOfOwner = await zombieContract.read.balanceOf([account]);

  async function zombieExists(zombieId) {
    try {
      await zombieContract.read.zombies([zombieId]);
      return true;
    } catch (error) {
      return false;
    }
  }

  const zombieIds = [];
  let currentZombieId = 0;

  while (true) {
    const exists = await zombieExists(currentZombieId);
    if (exists) {
      zombieIds.push(currentZombieId);
      currentZombieId++;
    } else {
      break; 
    }
  }

  const zombieInfos = [];

  for (const zombieId of zombieIds) {
    const info = await zombieContract.read.zombies([zombieId]);
    zombieInfos.push(info);
  }

  const myZombies = await zombieContract.read.getZombiesByOwner([account]);

  const ownerZombies = [];
  for (const zombieId of zombieIds) { 
    if (zombieId == myZombies) {
    const info = await zombieContract.read.zombies([zombieId]);
    ownerZombies.push(info);
    }
  }

  document.querySelector("#app").innerHTML = `
    <div>
      <h1>CryptoZombies</h1>
      <p>Current block number is ${blockNumber}</p>
      <p>Owner address is ${account}</p>
      <p>You are the owner of ${balanceOfOwner} zombie(s).</p>
      <p>Your zombie id is ${myZombies}.</p>

      <h2>My Zombies :</h2>
      <div id="galleryZombie">
      <span id="zombieEmpty"></span>
      </div>

      <h2>Zombie Gallery :</h2>
      <div id="galleryZombie">
      <span id="noZombies"></span>
      </div>

      <h2>Create a new zombie </h2>
      <div>
        <form>
          <label for="zombieName">Zombie's name : </label>
          <input type="text" name="zombieName" id="zombieName"/>
          <button id="createNewZ">Create </button>
        </form>
        <span id="newZombieSpan"></span>
      </div>

    </div>
  `;

  if (myZombies.length == 0) {
    document.querySelector("#zombieEmpty").innerHTML=`No zombies`;
  } else {
    document.querySelector("#zombieEmpty").innerHTML= `${ownerZombies.map((info, index) => 
      `<ul><h3>Zombie ${index + 1}: </h3>
      <li>Name: ${info[0]}</li>
      <li>DNA: ${info[1]}</li>
      <li>Level: ${info[2]}</li>
      <br/>
      <button id="levelMyZ">Level Up</button>
      <span id="levelUpSpan"></span>
      </ul>
      <span id="zombieView"></span>
      `).join('')}`;

      document.querySelector("#zombieView").innerHTML= `
      <div id="displayZombie">
      <img src="zombieparts/left-thigh-1@2x.png" id="left-thigh"></img>
      <img src="zombieparts/left-leg-1@2x.png" id="left-leg"></img>
      <img src="zombieparts/left-feet-1@2x.png" id="left-feet"></img>
      <img src="zombieparts/left-upper-arm-1@2x.png" id="left-upper-arm"></img>
      <img src="zombieparts/left-forearm-1@2x.png" id="left-forearm"></img>
      <img src="zombieparts/hand1-1@2x.png" id="left-hand"></img>
      <img src="zombieparts/right-thigh-1@2x.png" id="right-thigh"></img>
      <img src="zombieparts/right-leg-1@2x.png" id="right-leg"></img>
      <img src="zombieparts/right-feet-1@2x.png" id="right-feet"></img>
      <img src="zombieparts/right-upper-arm-1@2x.png" id="right-upper-arm"></img>
      <img src="zombieparts/right-forearm-1@2x.png" id="right-forearm"></img>
      <img src="zombieparts/hand-2-1@2x.png" id="right-hand"></img>
      <img id="shirt"></img>
      <img id="head"></img>
      <img id="eyes"></img>
      <img src="zombieparts/mouth-1@2x.png" id="mouth"></img>
      <img src="zombieparts/torso-1@2x.png" id="torso"></img>
      </div>`;

      document.getElementById("shirt").src = shirt(ownerZombies[0]);
      document.getElementById("head").src = head(ownerZombies[0]);
      document.getElementById("eyes").src = eyes(ownerZombies[0]);

      function shirt(zombie) {
        let currentDna = String(zombie[1]);
        let i = parseInt(currentDna.substr(4, 6)) % 6 + 1;
        return `zombieparts/shirt-${i}@2x.png`;
      }
    
      function head(zombie) {
        let currentDna = String(zombie[1]);
        let i = parseInt(currentDna.substr(0, 2)) % 7 + 1;
        return `zombieparts/head-${i}@2x.png`;
      }
    
      function eyes(zombie) {
        let currentDna = String(zombie[1]);
        let i = parseInt(currentDna.substr(2, 4)) % 11 + 1;
        return `zombieparts/eyes-${i}@2x.png`;
      }
    
      let LEVEL_UP_COST_AS_STRING = "0.001";

      document.querySelector("#levelMyZ").addEventListener("click", async (event) => {
        event.preventDefault();
        const hash = await zombieContract.write.levelUp([myZombies], {value: parseEther(LEVEL_UP_COST_AS_STRING)});
        document.querySelector("#levelUpSpan").innerHTML=`Payment in progress`;
        const transaction = await publicClient.waitForTransactionReceipt({hash: `${hash}`});
        if (transaction.status=="success") {
          document.querySelector("#levelUpSpan").innerHTML=`Leveled up complete`;
        } else {
          document.querySelector("#levelUpSpan").innerHTML=`Cannot level up`;
        };
      });
  }

  if (zombieInfos.length == 0) {
    document.querySelector("#noZombies").innerHTML=`Create a zombie`
  } else {
    document.querySelector("#noZombies").innerHTML= `${zombieInfos.map((info, index) => 
      `<ul><h3>Zombie ${index + 1}: </h3>
      <li>Name: ${info[0]}</li>
      <li>DNA: ${info[1]}</li>
      <li>Level: ${info[2]}</li>
      </ul>`).join('')
    
    }
      `;
  }

  document.querySelector("#createNewZ").addEventListener("click", async (event) => {
    event.preventDefault();
    const hash = await zombieContract.write.createRandomZombie([document.querySelector("#zombieName").value]);
    document.querySelector("#newZombieSpan").innerHTML=`Waiting for new zombie`;
    const transaction = await publicClient.waitForTransactionReceipt({hash: `${hash}`});
    if (transaction.status=="success") {
      document.querySelector("#newZombieSpan").innerHTML=`Zombie creation : success`;
    } else {
      document.querySelector("#newZombieSpan").innerHTML=`No new zombie`;
    };
  });

