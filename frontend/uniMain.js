import "./style.css";

// 1. Import modules.
import { createPublicClient, http, getContract, formatUnits, createWalletClient, custom, parseUnits, parseAbiItem, getAddress} from "viem";
import { goerli } from "viem/chains";
import { UNI } from "./abi/UNI";

// 2. Set up your client with desired chain & transport.
const publicClient = createPublicClient({
  chain: goerli,
  transport: http(),
});


const [account] = await window.ethereum.request({ method: "eth_requestAccounts" });
const walletClient = createWalletClient({
  account,
  chain: goerli,
  transport: custom(window.ethereum),
});

const uniContract = getContract({
  address : "0x1f9840a85d5aF5bf1D1762F925BDADdC4201F984",
  abi: UNI,
  publicClient,
  walletClient,
});

// 3. Consume an action!
const blockNumber = await publicClient.getBlockNumber();

const symbol = await uniContract.read.symbol();
const name = await uniContract.read.name();
const decimals = await uniContract.read.decimals();
const totalSupply = formatUnits(await uniContract.read.totalSupply(), decimals);
let balanceOf = formatUnits(await uniContract .read.balanceOf([account]));

//console.log(recipient);




//await uniContract.write.transfer(["0xc2ba723430706885144a37b2EC74feb82A0C1333", 1n]);

document.querySelector("#app").innerHTML = `
  <div>
    <p>Current block is 
    <span id="blockNumber"> 
    ${blockNumber}
      </span></p>
      <h1>${symbol} </h1>
      <p> Name : ${name} </br> 
      Decimals : ${decimals} </br>
      Total Supply : ${totalSupply} </br>
      Balance Of : <span id="balanceOf">${balanceOf}</span></br>
      Amount : <input type="text" value="0" id="amountInput"/> 
      <button type ="submit" id="maxButton"> MAX </button></br>
      recipient : <input type="text" value="0x" id="recipientInput" /> 
      <button type ="submit" id="sendButton"> SEND </button> </br>
      <span id="transaction"> </span>
      </p>
  </div>
`;

const unwatch = publicClient.watchBlockNumber( 
  { onBlockNumber: blockNumber => {
      //console.log(blockNumber); 
      document.querySelector("#blockNumber").innerHTML = `${blockNumber}`; 
    } }
);

document.querySelector("#maxButton").addEventListener("click", (event) => {
  event.preventDefault();
  document.querySelector("#amountInput").value = formatUnits(balanceOf, decimals);
});


document.querySelector("#sendButton").addEventListener("click", async () => {
  console.log(amountInput);
  const amount = parseUnits(document.querySelector("#amountInput").value, decimals);
  const recipient = document.querySelector("#recipientInput").value;
  const hash = await uniContract.write.transfer([recipient, amount]);
  document.querySelector("#transaction").innerHTML= "waiting for " + hash;
  const transaction = await publicClient.waitForTransactionReceipt( 
    { hash }
  );
  if(transaction.status == "success"){
    document.querySelector("#transaction").innerHTML= "Transaction " + hash + " confirmed !";
  }
  else{
    document.querySelector("#transaction").innerHTML= "Transaction " + hash + " failed !";
  };
});


publicClient.watchBlockNumber({
  onBlockNumber: async () => {
    const logs = await publicClient.getLogs({
      address: uniContract.address,
      event: parseAbiItem("event Transfer(address indexed from, address indexed to, uint256 value)"),
    });

    const myTransfers = logs.filter((log) => log.args.from == getAddress(account) || log.args.to == getAddress(account));

    if (myTransfers.length > 0) {
      balanceOf = await uniContract.read.balanceOf([account]);
      console.log(`New balance ${balanceOf}`);
      document.querySelector("#balanceOf").innerHTML = formatUnits(balanceOf, decimals);
    }
  }
})